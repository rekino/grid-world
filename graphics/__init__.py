__author__ = 'Ramin'

import cocos
import pyglet
from grid import GRID_SIX


class GridWorldLayer(cocos.tiles.RectMapLayer):
    def __init__(self, grid):
        cells = [[None] * grid.height for x in range(grid.width)]
        for j in xrange(grid.width):
            for i in xrange(grid.height):
                properties = self._get_properties(grid[i][j])
                image = pyglet.image.load(self._get_image_file(grid[i][j]))
                tile = cocos.tiles.Tile('{0},{1}'.format(i, j), {}, image)
                cells[j][i] = cocos.tiles.RectCell(j, i, 128, 128, properties, tile)

        cocos.tiles.RectMapLayer.__init__(self, 'world', grid.width, grid.height, cells)

    @staticmethod
    def _get_properties(cell):
        properties = {'top': True, 'right': True, 'down': True, 'left': True}

        if type(cell) == int or type(cell) == float:
            return properties

        for c in cell:
            if c == 'T':
                properties['top'] = False
            elif c == 'R':
                properties['right'] = False
            elif c == 'D':
                properties['down'] = False
            elif c == 'L':
                properties['left'] = False

        return properties

    @staticmethod
    def _get_image_file(cell):
        filename = '../resources/tile_{0}.png'

        if type(cell) == int or type(cell) == float:
            return filename.format('goal')

        if cell == ' ':
            return filename.format('free')

        return filename.format(cell.lower())

if __name__ == '__main__':
    cocos.director.director.init(width=768, height=768, do_not_scale=False, resizable=True)
    world_layer = GridWorldLayer(GRID_SIX)
    main_scene = cocos.scene.Scene(world_layer)
    world_layer.set_view(0, 0, world_layer.px_width, world_layer.px_height)

    cocos.director.director.run(main_scene)