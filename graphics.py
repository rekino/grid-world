__author__ = 'Ramin'

import cocos
from cocos import tiles, actions, layer
from cocos.director import director

director.init(width=768, height=768, do_not_scale=False, resizable=True)
car = cocos.sprite.Sprite('robot.png')

world_layer = tiles.load('grid.tmx')['world']
main_scene = cocos.scene.Scene(world_layer)
world_layer.set_view(0, 0, world_layer.px_width, world_layer.px_height)

director.run(main_scene)

