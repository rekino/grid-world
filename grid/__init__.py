__author__ = 'Ramin'

class Grid(object):
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.cells = [[' ' for i in xrange(height)] for j in xrange(width)]
        self.terminal_state = '#'
        self.start_state = None

    def __getitem__(self, row):
        return self.cells[row]

    def __setitem__(self, key, value):
        self.cells[key] = value

    def __str__(self):
        t = ''
        for i in reversed(xrange(self.width)):
            t += str(self.cells[i]) + '\n'
        return t


GRID_SIX = Grid(6, 6)
GRID_SIX.start_state = (0, 0)
GRID_SIX[4][1] = 'T'
GRID_SIX[5][1] = 'D'
GRID_SIX[1][2] = 'R'
GRID_SIX[2][2] = 'TR'
GRID_SIX[3][2] = 'D'
GRID_SIX[4][2] = 'T'
GRID_SIX[5][2] = 'D'
GRID_SIX[1][3] = 'L'
GRID_SIX[2][3] = 'L'
GRID_SIX[4][3] = 'T'
GRID_SIX[5][3] = 'D'
GRID_SIX[3][4] = 'R'
GRID_SIX[4][4] = 'R'
GRID_SIX[3][5] = 'L'
GRID_SIX[4][5] = 'L'
GRID_SIX[5][5] = 10

if __name__ == '__main__':
    print GRID_SIX[0][0]
    print
    print GRID_SIX[0]
    print
    print GRID_SIX