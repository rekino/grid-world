# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

import random
from base import Environment


class GridWorldEnviroment(Environment):
    def __init__(self, grid_world):
        self.grid_world = grid_world
        self.state = self.grid_world.getStartState()

    def getCurrentState(self):
        return self.state

    def getPossibleActions(self, state):
        return self.grid_world.getPossibleActions(state)

    def doAction(self, action):
        state = self.getCurrentState()
        successors = self.grid_world.getTransitionStatesAndProbs(state, action)
        total = 0.0
        rand = random.random()
        for nextState, prob in successors:
            total += prob
            if total > 1.0:
                raise Exception('Total transition probability more than one; sample failure.')
            if rand < total:
                reward = self.grid_world.getReward(state, action, nextState)
                self.state = nextState
                return nextState, reward
        raise Exception('Total transition probability less than one; sample failure.')

    def reset(self):
        self.state = self.grid_world.getStartState()