__author__ = 'Ramin'

from base import MarkovDecisionProcess
from bidict import bidict
from grid import GRID_SIX


class GridWorld(MarkovDecisionProcess):
    def __init__(self, grid):
        self.grid = grid
        self.noise = 0.2
        self.living_reward = 0.0

        self.action_results = bidict()
        self.action_results['T'] = (1, 0)
        self.action_results['R'] = (0, 1)
        self.action_results['D'] = (-1, 0)
        self.action_results['L'] = (0, -1)

    def getStates(self):
        states = [self.grid.terminal_state]
        for i in xrange(self.grid.height):
            for j in xrange(self.grid.width):
                state = (i, j)
                states.append(state)
        return states

    def getStartState(self):
        if self.grid.start_state is None:
            raise Exception('Grid has no start state')

        return self.grid.start_state

    def getPossibleActions(self, state):
        if self.isTerminal(state):
            return ()
        i, j = state
        if type(self.grid[i][j]) == int:
            return 'exit',

        return 'T', 'R', 'D', 'L'

    def getTransitionStatesAndProbs(self, state, action):
        if action not in self.getPossibleActions(state):
            raise Exception("Illegal action!")

        if self.isTerminal(state):
            return []

        i, j = state

        if type(self.grid[i][j]) == int or type(self.grid[i][j]) == float:
            return [(self.grid.terminal_state, 1.0)]

        successors = []

        di, dj = self.action_results[action]
        noise = self.noise

        if self._is_allowed(i+di, j+dj):
            if not self.action_results[:(di, dj)] in self.grid[i][j]:
                successors.append(((i+di, j+dj), 1 - noise))
            else:
                successors.append(((i, j), 1 - noise))
        else:
            successors.append(((i, j), 1 - noise))

        if self._is_allowed(i+dj, j+di):
            if not self.action_results[:(dj, di)] in self.grid[i][j]:
                successors.append(((i+dj, j+di), noise / 2))
            else:
                successors.append(((i, j), noise / 2))
        else:
            successors.append(((i, j), noise / 2))

        if self._is_allowed(i-dj, j-di):
            if not self.action_results[:(-dj, -di)] in self.grid[i][j]:
                successors.append(((i-dj, j-di), noise / 2))
            else:
                successors.append(((i, j), noise / 2))
        else:
            successors.append(((i, j), noise / 2))

        return successors

    def getReward(self, state, action, nextState):
        if self.isTerminal(state):
            return 0.0

        i, j = state
        cell = self.grid[i][j]
        if type(cell) == int or type(cell) == float:
            return cell

        return self.living_reward

    def isTerminal(self, state):
        return state == self.grid.terminal_state

    def _is_allowed(self, i, j):
        return 0 <= i < self.grid.height and 0 <= j < self.grid.width


if __name__ == '__main__':
    grid_world = GridWorld(GRID_SIX)
    grid_world.living_reward = -0.1

    print grid_world.getStates()
    print
    print grid_world.getStartState()
    print
    print grid_world.getPossibleActions((2, 2)), grid_world.getPossibleActions((5, 5)), grid_world.getPossibleActions('#')
    print
    print grid_world.getTransitionStatesAndProbs((0, 0), 'T'), grid_world.getTransitionStatesAndProbs((2, 2), 'T'), grid_world.getTransitionStatesAndProbs((5, 5), 'exit')
    print
    print grid_world.getReward((0, 0), 'T', (1, 0)), grid_world.getReward('#', 'T', (1, 0)), grid_world.getReward((5, 5), 'exit', '#')
